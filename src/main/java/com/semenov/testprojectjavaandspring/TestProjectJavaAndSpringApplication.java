package com.semenov.testprojectjavaandspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestProjectJavaAndSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestProjectJavaAndSpringApplication.class, args);
    }

}
