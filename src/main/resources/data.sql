-- Вставляем 10 примеров пользователей с указанными идентификаторами и хешированными паролями
INSERT INTO users (id, username, email, password)
VALUES (1, 'user1', 'user1@example.com', '$2a$12$D6bWPbwyWx.rSMM24cCSf.38zlzNHs09NqeH6Wx7N..IAtDFxdNM.'), -- Оригинальный пароль: password1
       (2, 'user2', 'user2@example.com', '$2a$12$Epa6g2Xr9QbWbsE4EaD/ce3MVZ9xM1B97EQPTWMrikm2Am/B5Nqla'), -- Оригинальный пароль: password2
       (3, 'user3', 'user3@example.com', '$2a$12$IAD6UDC0/asXaE//xICkMunSmzuISCey7sqwGnA2rJRrIGYzYuHfK'), -- Оригинальный пароль: password3
       (4, 'user4', 'user4@example.com', '$2a$12$tHHZFSsvNjZDVq5JHV69S.xdGuNvYfNTLsnjB.so6UAuE.7DAbKEK'); -- Оригинальный пароль: password4
