# Приложение авторизации и аутентификации на Spring Boot

Это приложение предоставляет функционал авторизации и аутентификации с использованием Spring Boot, Java 17, Spring Security, JWT, Docker, Docker Compose, Swagger и Postman.

## Требования

- Java 17
- Maven
- Docker
- Docker Compose

## Установка и запуск

1. Склонируйте репозиторий на свою локальную машину:

```
$ git clone https://gitlab.com/VladSemenovForVibeLab/test-project-java-and-spring.git
```

2. Перейдите в папку проекта:

```
$ cd TestPRojectJavaAndSpring
```

3. Соберите проект с помощью Maven:

```
$ mvn clean package
```

4. Создайте Docker:

```
$ docker compose up
```

5. Приложение будет доступно по следующему URL:

```
http://localhost:8080
```
## Диаграмма классов
![Диаграмма](docs/img.png)

## Использование

### Swagger

Вы можете открыть Swagger UI для тестирования API, перейдя по ссылке:

```
http://localhost:8080/swagger-ui.html
```

### Postman

В папке `postman` в проекте вы найдете коллекцию запросов Postman для тестирования API приложения.

## Аутентификация и авторизация

Для аутентификации и авторизации используется JSON Web Token (JWT).

Для аутентификации, выполните POST-запрос на эндпоинт `/api/auth/sigin`, передав данные пользователя в теле запроса в формате JSON:

```
{
  "username": "user1",
  "password": "password
}
```

После успешной аутентификации, вы получите ответ с JWT-токеном.

Для авторизации в защищенных эндпоинтах, добавьте полученный токен в заголовок `Authorization` с префиксом `Bearer`.

## Данные

Для начальной работы были добавлены следующие данные:
```
INSERT INTO users (id, username, email, password)
VALUES (1, 'user1', 'user1@example.com', '$2a$12$D6bWPbwyWx.rSMM24cCSf.38zlzNHs09NqeH6Wx7N..IAtDFxdNM.'), -- Оригинальный пароль: password1
       (2, 'user2', 'user2@example.com', '$2a$12$Epa6g2Xr9QbWbsE4EaD/ce3MVZ9xM1B97EQPTWMrikm2Am/B5Nqla'), -- Оригинальный пароль: password2
       (3, 'user3', 'user3@example.com', '$2a$12$IAD6UDC0/asXaE//xICkMunSmzuISCey7sqwGnA2rJRrIGYzYuHfK'), -- Оригинальный пароль: password3
       (4, 'user4', 'user4@example.com', '$2a$12$tHHZFSsvNjZDVq5JHV69S.xdGuNvYfNTLsnjB.so6UAuE.7DAbKEK'); -- Оригинальный пароль: password4
```

## Автор 
Семенов В.В